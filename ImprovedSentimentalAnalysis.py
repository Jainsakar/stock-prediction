# -*- coding: utf-8 -*-
"""
Created on Tue Feb  4 21:15:42 2020

@author: jains
"""
import requests
import bs4
import urllib
import webbrowser
import re
from nltk.sentiment.vader import SentimentIntensityAnalyzer as SIA
import nltk
from matplotlib import pyplot as plt
import datetime
from datetime import date
import time
import seaborn as sns
import math
import numpy as np
import pandas as pd
from sklearn.preprocessing import StandardScaler
from sklearn.metrics import mean_squared_error
from pycorenlp import StanfordCoreNLP
nltk.download('vader_lexicon')
sia=SIA()
#===============================================================================
#dataset
#===============================================================================
response = pd.read_html("https://finance.yahoo.com/quote/GOOG/history?period1=1555525800&period2=1571337000&interval=1d&filter=history&frequency=1d")
df = pd.DataFrame()
for data in response:
    df = data
df = df[:-1]
df = df.reindex(index=df.index[::-1])
datetime.datetime.strptime(df['Date'][0],'%b %d, %Y')
df.loc[:, 'Date'] = pd.to_datetime(df['Date'],format='%b %d, %Y')
df.columns = [str(x).lower().replace(' ', '_') for x in df.columns]
df.sort_values(by='date', inplace=True, ascending=True)
df['low'] = df['low'].astype(float)
df['open'] = df['open'].astype(float)
df['high'] = df['high'].astype(float)
df['adj_close**'] = df['adj_close**'].astype(float)
df.drop(['close*'],axis = 1,inplace = True)
df.drop(['volume'],axis = 1, inplace = True)
df_linear = df
df_csv = df
#===================================================================================
nlp = StanfordCoreNLP('http://localhost:9000') 
def get_sentiment(text):
    res = nlp.annotate(text,
                       properties={'annotators': 'sentiment',
                                   'outputFormat': 'json',
                                   'timeout': 100000,
                       })
    return (res['sentences'][0]['sentimentValue'])
final_scores = []
newsChannels = ["economictimes.indiatimes.com","www.ndtv.com","www.businesstoday.in","www.business-standard.com","www.livemint.com","www.financialexpress.com","www.indiatoday.in","finance.yahoo.com","timesofindia.indiatimes.com"]
def matchingLinks(newsChannels,link):
    for i in range(len(newsChannels)):
        found = re.search(newsChannels[i],link)
#        print(found)
        if(found):
#            print("working")
            return 1
    return 0 

for i in range(0,5):
    curr_date = df['date'][100-i-1]
#    print(type(curr_date))
    date_time = curr_date.strftime("%m-%d-%Y")
    print(date_time)
    url1='https://www.google.com/search?q='
    company_name='infosys'
    url2='&rlz=1C1CHZL_enIN820IN820&biw=1366&bih=625&sxsrf=ACYBGNR_Yffd_Zho481hN4gWm15kmPcsFA%3A1570634911397&source=lnt&tbs=cdr%3A1%2Ccd_min%3A'
    min_date=date_time.split('-')		# in m(m)-d(d)-yyyy
    url3=min_date[0]+'%2F'+min_date[1]+'%2F'+min_date[2]
    max_date=date_time.split('-')		# in m(m)-d(d)-yyyy
    url5=max_date[0]+'%2F'+max_date[1]+'%2F'+max_date[2]
    #print(url3)
    url4='%2Ccd_max%3A'
    url6='&tbm=nws'
    url=url1+company_name+url2+url3+url4+url5+url6
    #print(url)
    h={'User-Agent':'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/62.0.3202.89 Safari/537.36'}
    res=requests.get(url,headers=h)
    soup=bs4.BeautifulSoup(res.text,'lxml')
    #print(url5)
    links=soup.select('.r a')
    #print(links)
#    x=int(input('Enter the number of search results to be opened: '))
#    tabs=min(x,len(links))
    score=0
    scores=[]
    tabs = 6
    tabs = min(tabs,len(links))
    found = 0
#    pattern = "https://economictimes.indiatimes.com"
    for i in range(tabs):
#        print(links[i].get('href'))
        para=''
        r=requests.get(links[i].get('href'),headers=h)
        s=bs4.BeautifulSoup(r.text,'lxml')
#        print(links[i].get('href'));
        
        z = matchingLinks(newsChannels,links[i].get('href'))
#        print("this value of z : ",z)
        if(z==1):
            found = 1
            print(links[i].get('href'))
            if(re.search(newsChannels[0],links[i].get('href'))):
                mydivs = s.findAll("div", {"class": "section1"})
                for div in mydivs:
                    for i in div.find_all("div", {"class": "Normal"}):
                        para = i.getText()
                        para = para[:1000]
#                print(par)
            elif(re.search(newsChannels[4],links[i].get('href'))):
                mydivs = s.findAll("div", {"class": "story-content"})
                for div in mydivs:
                    for i in div.find_all("span",{"class" : "p-content paywall"}):
                        para = i.getText()
#                print(par)
            else:
                par=(s('p',limit=7))
                y=[re.sub(r'<.+?>',r'',str(a)) for a in par]
                para=''
                for i in y:
                    para=para+i
#            get_sentiment(para)
#            scores.append(sia.polarity_scores(para)['compound'])
#            para =  para.replace('"', '')
#            para =  para.replace('\'', '')
#            print("This line is executing : ",para)
            val = float(get_sentiment(para))
            scores.append(val)
##            print(sia.polarity_scores(para)['compound'] )
##            print("length: ",len(para));
##            print(para)
##            score+=sia.polarity_scores(para)['compound']
#            print(val)
#            print("========================ie=======================================")
    if(found == 0):
        scores = [2]   
    #score/=len(scores)
    #final_scores.append(score)
    final_scores.append(sum(scores)/len(scores))
#============================================================================================================







